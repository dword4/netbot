from slack_bolt import App
from slack_bolt.adapter.socket_mode import SocketModeHandler
import os
import re
from plugins import *
from dotenv import load_dotenv

load_dotenv()
sign_secret = os.environ.get('SIGNING_SECRET')
bot_token = os.environ.get('BOT_TOKEN')
app_token = os.environ.get('APP_TOKEN')

app = App(token=bot_token)

all_cmds = []

p = Plugins()
for plug in p.found_plugins_obj:
	for cmd in plug.Plugin().parts:
		all_cmds.append(cmd)

#	all_cmds.append({'obj': None, 'method': 'reload_plugin', 'command': '!reload_plugin'})

def reload_plugin(plugin_name):
	""" reload a specified plugin """
	#p = Plugins()
	p.reload_plugin(plugin_name)
	response = f"reloaded plugin: {plugin_name}"
	return response


@app.event("app_mention")
def event_test(say):
	say("Hellooooo there")

def get_help_command(obj, command):
	method_call = getattr(obj, command)
	return method_call.__doc__

'''
	generic parse command using regex to find any line that starts with !
'''
@app.command("/friday")
def cmd_friday(ack, respond, command):
	ack()
	args = command['text'].split(' ')
	c = args[0]
	if c == "ip":
		# ip 
		respond(f"Command: {c}")
	elif c == "dns":
		# dns
		respond(f"Command: {c}")
@app.message(r"(^!)")
def parse_command(message, say):
	args = message['text'].split(' ')
	c = args[0]
	for cmd in all_cmds:
		if c == cmd['command']:
			say(f"we found a hit")	
			method_call = getattr(cmd['obj'], cmd['method'])
			res = method_call()
			say(res)
		elif c == "!reload_plugin":
			#  reload a plugin
			pn = args[1]
			say(f"trying to reload {pn}")
			res = p.reload_plugin(pn)
			say(res)
		
			'''
			Something appears to be wrong here, when trigger a !reload_plugin admin from slack
			the bot's console shows it reloads/reinits the plugin and even shows a change to the 
			version number, but a subsequent !ver call to the bot on slack shows its still on 
			the previous version
			'''
		elif c == "!help":
			# check if there are arguments?
			if len(args) == 1:
				# single invocation of !help
				uid = message['user']
				help_commands = []
				for c in all_cmds:
					help_commands.append(c['command'])
				say(f"Commands: {help_commands}", channel=uid)
			elif len(args) == 2:
				# help on a specific command
				uid = message['user']
				for c in all_cmds:
					if c['command'] == args[1]:
						res = get_help_command(cmd['obj'], c['method'])
						say(f"Help for {args[1]}: {res}", channel=uid)	
			else:
				# longer, with sub commandss possibly?
				pass

if __name__ == "__main__":
	SocketModeHandler(app, app_token).start()
