class Plugin(object):

    def __init__(self):
        """ Hockey system """
        self.version = '0.1'
        self.parts = [
            {'obj': self, 'method': 'schedule', 'command': '!schedule', 'admin': False, 'help': 'self.schedule.__doc__'}
        ]
        print('initalize hockey features')

    def schedule(self):
        """ this is helpful for schedule() """
        return "Schedule()"

